#!/bin/sh
if [ "$#" -eq 0 ] || ([ ! -f "$1"i ] && [ ! -f "$2" ]); then
	echo 'The specified file does not exist'
	echo 'Help: runDnsEnumfromList <namefile> <dict>'
	exit 0
else
	for i in `cat $1`
	do
		echo "DnsEnum.pl is running for:" $i
		perl ./dnsenum.pl --enum -f $2 --noreverse --update -a $i | tee $i-dnsenum.txt
		if [ $? -eq 0 ]; then
			echo "Sucess execution of DnsEnum.pl for " $i
			echo "Output saved in the same directory where the script is run from"
		else
			echo "Fail execution of DnsEnum.pl for " $i
		fi
	done
fi
exit
